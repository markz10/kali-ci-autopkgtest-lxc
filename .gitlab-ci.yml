variables:
   WORKING_DIR: $CI_PROJECT_DIR/lxc
   ARTIFACTS_DIR: $CI_PROJECT_DIR/artifacts
   debci_quiet: 'false'

stages:
  - base
  - inherited

.build-base: &build-base
  stage: base
  image: registry.gitlab.com/kalilinux/tools/kali-ci-pipeline/autopkgtest
  script:
    - |
        cat >/etc/lxc/default.conf <<EOT
        lxc.net.0.type = veth
        lxc.net.0.link = lxcbr0
        lxc.net.0.flags = up
        lxc.net.0.hwaddr = 00:16:3e:xx:xx:xx
        lxc.apparmor.profile = unconfined
        EOT
    - | 
        cat >/etc/default/lxc-net <<EOT
        USE_LXC_BRIDGE="true"
        EOT
    - |
        cat >/etc/lxc/lxc.conf <<EOT
        lxc.lxcpath=${WORKING_DIR}
        EOT
    - /etc/init.d/lxc-net restart || true
    - mount -t tmpfs cgroup_root /sys/fs/cgroup
    - mkdir /sys/fs/cgroup/cpuset
    - mkdir /sys/fs/cgroup/devices
    - mount -t cgroup cpuset -o cpuset /sys/fs/cgroup/cpuset/
    - mount -t cgroup devices -o devices /sys/fs/cgroup/devices
    - /etc/init.d/lxc start
    - /etc/init.d/lxc restart
    - /etc/init.d/libvirtd start
    - virsh net-start default
    - |
        if [ ! -e /usr/share/debci/bin/debci-generate-apt-sources ]; then
            for p in $(find ${CI_PROJECT_DIR}/patches -name \*.patch | sort); do
                patch -p1 -d /usr/share/debci <$p;
            done
        fi
    #- debci setup --suite ${CI_JOB_NAME} --backend lxc --mirror http://http.kali.org
    # Work around the failure of the above command by creating a testing
    # container and transforming it into a Kali container.
    # See https://gitlab.com/kalilinux/internal/autopkgtest.kali.org/issues/14
    - debci setup --suite testing --backend lxc
    - |
        cat >${WORKING_DIR}/autopkgtest-testing-amd64/rootfs/etc/apt/sources.list <<EOT
        deb http://http.kali.org/kali ${CI_JOB_NAME} main contrib non-free
        deb-src http://http.kali.org/kali ${CI_JOB_NAME} main contrib non-free
        EOT
    - rm -rf ${WORKING_DIR}/autopkgtest-testing-amd64/rootfs/etc/apt/sources.list.d/*
    - curl https://archive.kali.org/archive-key.asc > ${WORKING_DIR}/autopkgtest-testing-amd64/rootfs/etc/apt/trusted.gpg.d/kali-archive-key.asc
    - chroot ${WORKING_DIR}/autopkgtest-testing-amd64/rootfs apt-get update
    - chroot ${WORKING_DIR}/autopkgtest-testing-amd64/rootfs apt-get install -y kali-archive-keyring
    - chroot ${WORKING_DIR}/autopkgtest-testing-amd64/rootfs apt-get dist-upgrade -y
    - chroot ${WORKING_DIR}/autopkgtest-testing-amd64/rootfs apt-get clean
    - rm -f ${WORKING_DIR}/autopkgtest-testing-amd64/rootfs/etc/apt/trusted.gpg.d/kali-archive-key.asc
    - mv ${WORKING_DIR}/autopkgtest-testing-amd64 ${WORKING_DIR}/autopkgtest-${CI_JOB_NAME}-amd64
    # Fix hostname & paths in multiple places
    - sed -i -e "s/autopkgtest-testing/autopkgtest-${CI_JOB_NAME}/" ${WORKING_DIR}/autopkgtest-${CI_JOB_NAME}-amd64/config ${WORKING_DIR}/autopkgtest-${CI_JOB_NAME}-amd64/rootfs/etc/hostname
    # Fix vendor symlink
    - rm -f ${WORKING_DIR}/autopkgtest-${CI_JOB_NAME}-amd64/rootfs/etc/dpkg/origins/default
    - ln -sf kali ${WORKING_DIR}/autopkgtest-${CI_JOB_NAME}-amd64/rootfs/etc/dpkg/origins/default

  after_script:
    - mkdir -p ${ARTIFACTS_DIR}
    - tar -cf ${ARTIFACTS_DIR}/lxc.tar --exclude /dev -C ${WORKING_DIR} .

  artifacts:
    paths:
    - ${ARTIFACTS_DIR}

.build-inherited: &build-inherited
  stage: inherited
  image: debian
  # dependencies:
  #   - '<base-suite>'
  script:
    - mkdir -p "${WORKING_DIR}"
    - tar -C "${WORKING_DIR}" -xf "${ARTIFACTS_DIR}/lxc.tar"
    - BASENAME=$(ls -1 "${WORKING_DIR}" | sed -e 's/^autopkgtest-//' -e 's/-amd64$//')
    - mv "${WORKING_DIR}"/autopkgtest-${BASENAME}-amd64 "${WORKING_DIR}/autopkgtest-${CI_JOB_NAME}-amd64"
    - echo "deb http://http.kali.org/kali ${CI_JOB_NAME} main" > "${WORKING_DIR}/autopkgtest-${CI_JOB_NAME}-amd64/rootfs/etc/apt/sources.list.d/sci.list"
    - sed -i "s/${BASENAME}/${CI_JOB_NAME}/g" "${WORKING_DIR}/autopkgtest-${CI_JOB_NAME}-amd64/config"
    - tar -cf "${ARTIFACTS_DIR}/lxc.tar" --exclude /dev -C "${WORKING_DIR}" .
  artifacts:
    paths:
    - ${ARTIFACTS_DIR}

kali-experimental:
  extends: .build-inherited
  dependencies:
    - kali-dev

kali-dev:
  extends: .build-base

kali-rolling:
  extends: .build-base
